<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'nis' => '11111111',
            'email' => 'admin@example.com',
            'username' => 'admin',
            'level' => 'admin',
            'password' => bcrypt('password')

        ]);
        \App\Models\User::factory(10)->create();
        DB::table('perusahaans')->insert([
            [
                'nama_perusahaan' => 'PT. POS Indonesia',
                'alamat_perusahaan' => 'Dago Pojok'
            ],
            [
                'nama_perusahaan' => 'PT. Karya Eka Nala',
                'alamat_perusahaan' => 'Dago Giri'
            ],
        ]);
        DB::table('jurusans')->insert([
            [
                'jurusan' => 'Rekayasa Perangkat Lunak',
                'kepala_jurusan' => 'Eron Verbena'
            ],
            [
                'jurusan' => 'Listrik',
                'kepala_jurusan' => 'guru listrik'
            ],
        ]);
    }
}
