<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('nis');
            $table->string('kelas')->nullable();
            $table->string('email')->unique();
            $table->foreignId('jurusan_id')->nullable();
            $table->foreignId('pembimbing_id')->nullable();
            $table->foreignId('perusahaan_id')->nullable();
            $table->foreignId('jurnal_id')->nullable();
            $table->foreignId('laporan_id')->nullable();
            $table->string('kehadiran')->nullable();
            $table->string('foto_users')->nullable();
            $table->date('mulai_pkl')->nullable();
            $table->date('selesai_pkl')->nullable();
            $table->string('pengantar_pkl')->nullable();
            $table->string('ket_message_pengantar')->nullable();
            $table->string('level');
            $table->string('status')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
