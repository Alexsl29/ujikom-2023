@extends('layouts.siswa.dashboard')

@section('body')

    <div class="container">
        <div class="card">
            <div class="card-body">
                <h3>Form Edit </h3><hr>
                <form action="/book/update" method="post">
                    @csrf
                    @foreach ($data as $item)
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    <div class="mb-3">
                        <label for="" class="form-label">Name :</label>
                        <input type="text"class="form-control" name="name" value="{{ $item->Name }}">
                        <small id="helpId" class="form-text text-muted">Nama Buku</small>
                    </div>
                    <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                    @endforeach
                    <a href="/book" class="btn btn-secondary">Back</a>
    </div>    
                </form>
            </div>
        </div>

@endsection