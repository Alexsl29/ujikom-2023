<nav class="navbar navbar-expand d-flex flex-column align-item-start" id="sidebar">

  <a href="#" class="navbar-brand text-light mt-5">
    <div class="display-5 font-weight-bold">
      <span class="bi bi-person-circle"> SMK Mutiara Kota Bandung</span>
    </div>
  </a>

  <ul class="navbar-nav d-flex flex-column mt-5 w-100">
    <li class="nav-item w-100">
      <a href="/" class="nav-link text-dark pl-4 text-decoration-none"><span class="bi bi-laptop"> Dashboard</span></a>
    </li>
  
    <li class="nav-item w-100">
      <a href="#" class="nav-link text-dark pl-4 text-decoration-none"><span class="bi bi-people"> User</span></a>
    </li>
    
    <li class="nav-item dropdown w-100">
      <a href="#" class="nav-link dropdown text-dark pl-4 text-decoration-none" id="navbarDropdown" role="button" data-toggle="dropdown"
      aria-expanded="false"><span class="bi bi-table"> Tabel</span><span class="bi bi-chevron-down" style="float: right; margin-right: 12px"></span></a>
      <ul class="dropdown-menu w-100" aria-labelledby="navbarDropdown">
        <li><a href="/perusahaan" class="dropdown-item pl-4 p-2">Tabel Perusahaan</a></li>
        <li><a href="/pembimbing" class="dropdown-item pl-4 p-2">Tabel Pembimbing</a></li>
        <li><a href="/table" class="dropdown-item pl-4 p-2">Tabel Siswa</a></li>
      </ul>
    </li>
    
  </ul>

</nav>