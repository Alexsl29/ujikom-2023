@extends('layouts.pembimbing.dashboard')

@section('body')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-inverse table-inverse table-responsive text-center">
                    <thead class="thead-inverse|thead-default">
                        <tr >
                            <th class="text-center">NIS</th>
                            <th class="text-center">Nama Lengkap</th>
                            <th class="text-center">Nama Pembimbing</th>
                            <th class="text-center">Progress</th>
                            <th class="text-center">aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            @if ($item->status == "Menunggu Proses Validasi Surat Pengantar" && "Pengajuan Prakerin Disetujui")
                                
                            <tr>
                                <td scope="row">{{ $item->nis }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    @if ($item->pembimbing_id == null)
                                        Siswa Belum Memiliki Pembimbing
                                    @else
                                        {{ $item->pembimbing->name }}
                                    @endif
                                </td>
                                <td>{{ $item->status }}</td>
                                <td>
                                    <a href="/validasiPengantar/{{ $item->id }}" class="btn btn-info btn-sm">Validasi Surat Pengantar</a>
                                    <a href="/detail/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                                </td>
                            </tr>
                            @elseif ($item->status == "Pengajuan Prakerin Disetujui")
                                <tr>
                                <td scope="row">{{ $item->nis }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    @if ($item->pembimbing_id == null)
                                        Siswa Belum Memiliki Pembimbing
                                    @else
                                        {{ $item->pembimbing->name }}
                                    @endif
                                </td>
                                <td>{{ $item->status }}</td>
                                <td>
                                    <a href="/detail/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Tetapkan Pembimbing</button>
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Tetapkan Pembimbing</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="/addPembimbing" method="post">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $item->id }}">
                                                <input type="hidden" name="status" value="Sudah Mendapatkan Pembimbing">
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Nama :</label>
                                                <input type="text" class="form-control" id="recipient-name" value="{{ $item->name }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Pilih Pembimibing :</label>
                                                <select class="custom-select" id="inputGroupSelect01" name="pembimbing_id">
                                                    <option selected value="">--Pilih Pembimbing--</option>
                                                    @foreach ($data1 as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                </table>
             <button onclick="kembali()" class="btn btn-danger">Kembali</button>
                <script>function kembali(){
                    window.history.back();
                }</script>
            </div>
        </div>
            
        
     </div>
@endsection