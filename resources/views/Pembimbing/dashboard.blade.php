@extends('layouts.pembimbing.dashboard')

@section('body')

  <div class="row">

    <div class="col-lg-5 d-flex justify-content-center">
      <img src="/img/4.jpg" alt="" id="image" style="width: 470px">
    </div>

    <div class="col-lg-7 mt-4">
      
      <div class="ml-5 mt-3">
        <span class="text-uppercase" id="title" style="font-weight: 500">menu</span>
      </div>
      
      <div class="row" id="menu" style="margin-left: 33px; margin-top: 50px">
        {{-- <div class="col-lg-3" id="kegiatan">
          <a href="/dashboard/jurnalSiswa" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/jurusan.svg" alt="" style="margin-top: 7px; margin-left: 1px"></a>
        </div>
        <div class="col-lg-3" id="pengajuan">
          <a href="/dashboard/Pengajuan_PKL" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/pengajuanPKL.svg" alt="" style="margin-top: 6px; margin-right: 1px"></a>
        </div>
        <div class="col-lg-3" id="monitoring">
          <a href="/dashboard/monitoring" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/profile.svg" alt="" style="margin-top: 7.8px"></a>
        </div>
        <div class="col-lg-3" id="tabel">
          <a href="/table" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/table.svg" alt="" style="margin-top: 7.8px"></a>
        </div>
          <div class="col-lg-3" id="tabel">
          <a href="/cekLaporan" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 30px; border-radius: 10px"><img src="/svg/table.svg" alt="" style="margin-top: 7.8px"></a>
        </div>
      </div>

      <div class="row" id="label">
        <div class="col-lg-3 text-center" style="margin-left: 7px">
          <span id="cekKegiatan">Cek Kegiatan<br>Siswa</span>
        </div>
        <div class="col-lg-3 text-center" style="margin-left: -12px">
          <span id="pengajuanPKL">Pengajuan<br>PKL</span>
        </div>
        <div class="col-lg-3 text-center" style="margin-left: -11px">
          <span id="monitoringLabel">Monitoring</span>
        </div>
        <div class="col-lg-3 text-center" style="margin-left: -10px">
          <span id="tabelLabel">Tabel</span>
        </div>
        <div class="col-lg-3 text-center" style="margin-left: -10px">
          <span id="tabelLabel">cek laporan siswa</span>
        </div>
      </div> --}}
        <div class="col-xs-7" id="Jurnal">
        <a href="/dashboard/jurnalSiswa" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 100px; border-radius: 10px"><img src="/svg/jurusan.svg" alt="" style="margin-top: 7px; margin-left: 1px"></a>
        <span id="labelPilihJurusan" class="mb-3"><br>cek kegiatan<br>siswa</span>
      </div>
      <div class="col-xs-7 text-end" id="pengajuan">
        <a href="/dashboard/Pengajuan_PKL" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 100px; border-radius: 10px"><img src="/svg/pengajuanPKL.svg" alt="" style="margin-top: 6px; margin-right: 1px"></a>
        <span id="labelPengajuanPKL"><br>Pengajuan<br>PKL</span>
      </div>
      <div class="col-xs-4" id="profile">
        <a href="/dashboard/monitoring" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 100px; border-radius: 10px"><img src="/svg/profile.svg" alt="" style="margin-top: 7.8px"></a>
        <span id="labelProfileSiswa"><br>Monitoring</span>
      </div>
      <div class="col-xs-4" id="informasi">
        <a href="/table" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 70px; border-radius: 10px"><img src="/svg/table.svg" alt="" style="margin-top: 7.8px"></a>
        <span id="labelInformasi"><br>Tabel</span>
      </div>
      <div class="col-xs-4" id="informasi">
        <a href="/cekLaporan" class="btn text-dark" style="background-color: #0590D8; width: 60px; height:60px; margin-right: 70px; border-radius: 10px"><img src="/svg/table.svg" alt="" style="margin-top: 7.8px"></a>
        <span id="labellaporan"><br>cek laporan <br>siswa</span>
      </div>
    </div>
 
  
      <div class="row" id="kembali">
        <div class="col-lg-12 d-flex justify-content-end mr-2 mb-2" style="margin-top: 112px; margin-left: -66px">
          <a href="#" class="btn btn-md" style="background-color: #0590D8; color: black">Kembali</a>
        </div>
      </div>

    </div>

  </div>

@endsection