@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Auth()->user()->level == 'admin')
            <a href="/createPembimbing" class="Button button-success">+ Tambah Pembimbing</a>
        @endif
        <table class="table  table-inverse table-responsive">
            <thead class="thead-inverse|thead-default">
                <tr>
                    <th>NIP</th>
                    <th>Nama lengkap</th>
                    <th>Kejuruan</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($data as $i)
                    <tr>
                        <td>{{ $i->nip }}</td>
                        <td scope="row">{{ $i->name }}</td>
                        <td>{{ $i->kejuruan }}</td>
                        <td>
                            <a href="/detailPembimbing/{{ $i->id }}" class="Button button-success">Detail</a>
                            @if (Auth()->user()->level == 'admin')
                            <a href="/editPembimbing/{{ $i->id }}" class="Button button-success">Edit</a>
                            <a href="/deletePembimbing/{{ $i->id }}" class="Button button-success">Delete</a>
                            @endif
                        </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
     </div>
@endsection