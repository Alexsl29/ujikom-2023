@extends('layouts.siswa.dashboard')

@section('body')

    <div class="container mt-4">
        <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#tambah" data-whatever="@mdo">Tambah</button>
        <div class="card">
    
                <table class="table table-bordered ">
                        <tr>
                            <th>Nama</th>
                            <th >Aksi</th>
                        </tr>
                        <tbody>
                            @foreach ($data as $item)
                            @foreach ($item->computer as $computer)
                                
                            <tr>
                                
                                <td>{{ $computer->name }}</td>
                                <td>
                                    <a href="/computer/edit/{{ $computer->id }}" type="button" class="btn btn-warning" >Edit</a>
                                    <a href="/computer/delete/{{ $computer->id }}" type="button" class="btn btn-danger" >Delete</a>
                                    
                                </td>
                            </tr>
                            @endforeach
                            @endforeach
                            
                        </tbody>
                </table>
                
            </div>
        <button onclick="kembali()" class="btn btn-danger">Kembali</button>
                <script>function kembali(){
                    window.history.back();
                }</script>
        </div>
    </div>

    {{-- Modal --}}
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Form tambah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/jurnal/tambah" method="post">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nama :</label>
                        <input type="date" class="form-control" id="recipient-name" name="tanggal">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                </div>
                </div>
            </div>
            </div>

            

@endsection