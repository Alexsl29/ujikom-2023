@extends('layouts.siswa.dashboard')

@section('body')

    <div class="container">
        <div class="card">
            <div class="card-body">
                <h3>Form Edit </h3><hr>
                <form action="/computer/update" method="post">
                    @csrf
                    @foreach ($data as $item)
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    <div class="mb-3">
                        <label for="" class="form-label">Nama :</label>
                        <input type="date"class="form-control" name="tanggal" value="{{ $item->tanggal }}">
                        <small id="helpId" class="form-text text-muted">Nama Buku</small>
                    </div>
                   
                    </div>
                    <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                    @endforeach
                    <a href="/computer" class="btn btn-secondary">Back</a>
                </form>
            </div>
        </div>
    </div>    

@endsection