@extends('layouts.siswa.dashboard')

@section('body')
    <div class="container mt-3">
        @if (Auth()->User()->laporan_id == null)  
            <div class="card">
                <div class="card-header">
                    Penyusunan Laporan Prakerin
                </div>
                <div class="card-body">
                    <h3>Silahkan Susun Laporan mulai dari sekarang</h3>
                    <form action="/createLaporan" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ Auth()->User()->id }}">
                        <input type="hidden" name="nis" value="{{ Auth()->User()->nis }}">
                        <input type="hidden" name="keterangan" value="Perlu di validasi">
                        <div class="mb-3">
                            <label for="" class="form-label">Upload Laporan Prakerin</label>
                        <input type="file"
                            class="form-control" name="file_laporan" id="" aria-describedby="helpId" placeholder="">
                            <small id="helpId" class="form-text text-muted">Laporan yang di upload akan segera di validasi oleh pembimbing</small>
                        </div>
                        <input type="submit" value="submit" class="btn btn-primary">
                    </form>
                <button type="button" class="btn btn-primary" style="float: right" data-toggle="modal" data-target="#exampleModalCenter">
                Baca Tata cara Penyusunan Laporan
                </button>
                    @foreach ($data as $item)
                    <a href="/download/{{ $item->file_laporan }}" class="btn btn-outline-info " style="float: right">Download Contoh Laporan PRAKERIN</a>
                    @endforeach
                </div>
            </div>
            @elseif  (Auth()->User()->laporan->keterangan == "Perlu di Revisi")
            <div class="card">
                <div class="card-header">Revisi Laporan penyusunan PRAKERIN</div>
                <div class="card-body">
                    <h4>Pembimbing telah mengecek laporan yang anda upload</h4>
                    <h5>Laporan yang anda upload perlu di <strong>Revisi</strong></h5><br>
                        <div class="mb-3">
                        <label for="" class="form-label">Catatan Dari Pembimbing</label>
                        <textarea class="form-control" name="" id="" rows="3" readonly>{{ Auth()->User()->laporan->coment }}</textarea>
                        </div>
                    <a href="/download/{{ Auth()->User()->laporan->file_laporan }}" class="btn btn-info">Download Laporan PRAKERIN </a>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadLaporan" data-whatever="@mdo">Upload Laporan Baru</button>
                    <br>
                        <div class="modal fade" id="uploadLaporan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="/updateLaporan" method="post" enctype="multipart/form-data">
                                    @csrf
                                        <input type="hidden" name="id" value="{{ Auth()->User()->laporan->id }}">
                                        <input type="hidden" name="keterangan" value="Perlu di validasi">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Upload Laporan:</label>
                                            <input type="file" class="form-control" id="recipient-name" name="file_laporan">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                </div>
                        </div>
</div>
                    @foreach ($data as $item)
                    <a href="/download/{{ $item->file_laporan }}" class="btn btn-outline-info " style="float: right">Download Contoh Laporan PRAKERIN</a>
                    @endforeach        
                    <button class="btn btn-primary mt-2">Baca Tata cara pembuatan Laporan</button>
                </div>
            </div>
            @elseif (Auth()->User()->laporan->keterangan == 'Laporan Siap di cetak')
                <div class="card">
                    <div class="card-header">Laporan Success</div>
                    <div class="card-body">
                        <h4>Laporan PRAKERIN anda sudah benar !</h4>
                        Silahkan cetak/printout Laporan Prakerin anda Lalu ajukan tanda tangan ke perusahaan<br>
                        <a href="/download/{{ Auth()->User()->laporan->file_laporan }}" class="btn btn-primary">Download Laporan PRAKERIN</a>
                    </div>
                </div>
            @elseif (Auth()->User()->laporan->keterangan == 'Perlu di validasi')
            <div class="card">
                <div class="card-header">Menunggu validasi laporan PRAKERIN</div>
                <div class="card-body">
                    <h5>Laporan Anda Berhasil Dikirim!</h5>
                    Tunggu laporan di validasi oleh pembimbing
                </div>
            </div>
            @else
            <div class="card">
                <div class="card-header">Menunggu validasi laporan PRAKERIN</div>
                <div class="card-body">
                    <h5>Laporan Anda Berhasil Dikirim!</h5>
                    Tunggu laporan di validasi oleh pembimbing
                </div>
            </div>
        @endif
            <button onclick="kembali()" class="btn btn-danger">Kembali</button>
            <script>function kembali(){
                window.history.back();
            }</script>
    </div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          @foreach ($data as $item)
               <textarea name="" id="" cols="30" rows="10">{{ $item->keterangan }}</textarea>
          @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection