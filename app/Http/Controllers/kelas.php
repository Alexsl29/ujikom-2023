<?php

namespace App\Http\Controllers;

use App\Models\kelas;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class kelasController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('kelas.Index', ['data' => $data]);
    }
    public function tambah(Request $request)
    {
        DB::table('kelas')->insert([
            'name' => $request->name,
        ]);
        return redirect()->back();
    }
    public function edit($id)
    {
        $data = kelas::all()->where('id', $id);
        return view('kelas.edit', ['data' => $data]);
    }
    public function update(Request $request)
    {
        DB::table('kelas')->where('id', $request->id)->update([
            'name' => $request->name,
        ]);
        return redirect('/kelas');
    }
}
