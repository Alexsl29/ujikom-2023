<?php

namespace App\Http\Controllers;

use App\Models\pembayaran;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class pembayaranController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('pembayaran.Index', ['data' => $data]);
    }
    public function tambah(Request $request)
    {
        DB::table('pembayaran')->insert([
            'name' => $request->name,
        ]);
        return redirect()->back();
    }
    public function edit($id)
    {
        $data = pembayaran::all()->where('id', $id);
        return view('pembayaran.edit', ['data' => $data]);
    }
    public function update(Request $request)
    {
        DB::table('pembayaran')->where('id', $request->id)->update([
            'name' => $request->name,
        ]);
        return redirect('/pembayaran');
    }
}
